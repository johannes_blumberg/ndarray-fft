use super::fft_impl::*;
use super::num_trailts_h::*;
use crate::fft_kd::_rfft_bwd_k;
use crate::fft_run::fft_inplace;

use ndarray::{Array, ArrayBase, Data, Dimension, RemoveAxis};

pub fn _fft_fb_n<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    ortho: bool,
    inverse: bool,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    let icnt = a.ndim();
    match n {
        Some(nlist) => {
            assert_eq!(
                nlist.len(),
                icnt,
                "Specified number of axes {} missmatches given dimension {}",
                nlist.len(),
                icnt
            );
            if icnt == 0 {
                return a.to_owned();
            }
            if icnt == 1 {
                return _fft_fb_axis(a, Some(nlist[0]), 0, ortho, inverse);
            }

            // icnt > 1
            let mut buffer = _fft_fb_axis(a, Some(nlist[0]), 0, ortho, inverse);
            for i in 1..icnt {
                let mut ni = Some(nlist[i]);
                buffer = make_ordered_copy(i, &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    inverse,
                );
            }

            buffer
        }
        None => {
            if icnt == 0 {
                return a.to_owned();
            }
            if icnt == 1 {
                return _fft_fb_axis(a, None, 0, ortho, inverse);
            }

            // icnt > 1
            let mut buffer = _fft_fb_axis(a, None, 0, ortho, inverse);
            for i in 1..icnt {
                let mut ni = None; // Will be fixed by the buffer creation routine
                buffer = make_ordered_copy(i, &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    inverse,
                );
            }

            buffer
        }
    }
}

// Compute the one-dimensional discrete real input Fouier Transform
pub fn _rfft_fwd_n<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    ortho: bool,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    let icnt = a.ndim();
    match n {
        Some(nlist) => {
            assert_eq!(
                nlist.len(),
                icnt,
                "Specified number of axes {} missmatches given dimension {}",
                nlist.len(),
                icnt
            );
            if icnt == 0 {
                panic!("Must specify at least one dimension")
            }
            if icnt == 1 {
                return _rfft_fwd_axis(a, Some(nlist[0]), 0, ortho);
            }

            // icnt > 1
            let mut buffer = _rfft_fwd_axis(a, Some(nlist[0]), 0, ortho);

            // Now it's just a normal Fouier Transform
            for i in 1..icnt {
                let mut ni = Some(nlist[i]);
                buffer = make_ordered_copy(i, &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    false,
                );
            }

            buffer
        }
        None => {
            if icnt == 0 {
                panic!("Must specify at least one dimension")
            }
            if icnt == 1 {
                return _rfft_fwd_axis(a, None, 0, ortho);
            }

            // icnt > 1
            let mut buffer = _rfft_fwd_axis(a, None, 0, ortho);

            // Now it's just a normal Fouier Transform

            // However we handcode it to avoid using an other buffer
            for i in 1..icnt {
                let mut ni = None; // Will be selected by the buffer creation routine
                buffer = make_ordered_copy(i, &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    false,
                );
            }

            buffer
        }
    }
}

// Compute the one-dimensional discrete real input Fouier Transform
pub fn _rfft_bwd_n<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: &[usize], ortho: bool) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    // We use a
    let axes: Box<[usize]> = (0..(a.ndim())).collect();
    _rfft_bwd_k(a, n, &axes, ortho)
}

/// Applies a discrete Fouier Transform along all axes
///
/// The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1.
///
/// This function should be used together with [`ifftn`]
pub fn fftn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: Option<&[usize]>) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_n(a, n, false, false)
}

/// Applies a discrete Inverse Fouier Transform along all axes
///
/// The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/n, where n is the number of elements along this axis.
///  
/// This function should be used together with [`fftn`]
pub fn ifftn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: Option<&[usize]>) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_n(a, n, false, true)
}

/// Applies a orthonormal discrete Fouier Transform along all axes
///
/// The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`oifftn`]
pub fn offtn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: Option<&[usize]>) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_n(a, n, true, false)
}

/// Applies a orthonormal discrete inverse Fouier Transform along all axes
///
/// The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`offtn`]
pub fn oifftn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: Option<&[usize]>) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_n(a, n, true, true)
}

// Real Space Transform

/// Applies a discrete Real Input Fouier Transform along all axes
///
/// The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1.
///
/// The real to complex transform is calculated along the first axis,
/// and the vector lengh along this axis is reduced correspondingly for the output.
/// If k is the lengh of the corresponding vector in the input arry,
/// the resulting lengh is 1+k/2 if k is even and (k+1)/2 if k is odd.
///
/// This function should be used together with [`irfftn`]
pub fn rfftn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: Option<&[usize]>) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    _rfft_fwd_n(a, n, false)
}

/// Applies a discrete Inverse Real Input Fouier Transform along all axes
///
/// The parameter n indecates the assumed vector lengh along the
/// transformation axes. It should specify the lengh for each transformation
/// dimension in the order given by axes as the show up in the resulting array.
/// The normalisation is taken to be 1/n, where n is the number of elements along this axis.
///
/// It is assumend that the first axis is the one that has been transformed using the real to complex
/// transform.
///  
/// This function should be used together with [`rfftn`]
pub fn irfftn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: &[usize]) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _rfft_bwd_n(a, n, false)
}

/// Applies a orthonormal discrete Fouier Transform along all axes
///
/// The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// The real to complex transform is calculated along the first axis,
/// and the vector lengh along this axis is reduced correspondingly for the output.
/// If k is the lengh of the corresponding vector in the input arry,
/// the resulting lengh is 1+k/2 if k is even and (k+1)/2 if k is odd.
///
/// This function should be used together with [`oirfftn`]
pub fn orfftn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: Option<&[usize]>) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    _rfft_fwd_n(a, n, true)
}

/// Applies a orthonormal discrete inverse Fouier Transform along all axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The parameter n indecates the assumed vector lengh along the
/// transformation axes. It should specify the lengh for each transformation
/// dimension in the order given by axes as the show up in the resulting array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// It is assumend that the first axis specified is the one that has been transformed using the real to complex
/// transform.
///
/// This function should be used together with [`orfftn`]
pub fn oirfftn<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: &[usize]) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _rfft_bwd_n(a, n, false)
}
