//use num_trails::identities::Zero;
//use num_complex;

mod num_trailts_h {
    pub use num_complex::Complex;
    use num_traits::cast::FromPrimitive;
    use num_traits::float::{Float, FloatConst};
    use num_traits::NumAssign;

    pub use num_traits::identities::Zero;

    /// Trail that is implemented for all types that meet the requierments
    pub trait FFTnum: Float + FloatConst + NumAssign + FromPrimitive {}
    impl<T> FFTnum for T where T: Float + FloatConst + NumAssign + FromPrimitive {}
}

pub use num_trailts_h::*;

mod fft_impl;
mod fft_run;

mod fft_1d;
pub use fft_1d::*;

mod fft_kd;
pub use fft_kd::*;

mod fft_nd;
pub use fft_nd::*;

#[cfg(test)]
mod tests {
    use super::fft_impl::make_ordered_copy;
    use super::*;
    use chfft::RFft1D;
    use ndarray::*;

    #[test]
    fn chfft_test() {
        let input = [2.0, 0.0, 1.0, 1.0, 0.0, 3.0, 2.0, 4.0];
        let mut fft = RFft1D::<f64>::new(input.len());
        let output = fft.forward(&input);
        let reoutput = fft.backward(&output);

        println!("the transform of {:?} is {:?}", input, output);
        println!("the transform of {:?} is {:?}", output, reoutput);

        println!("lenghs {} {} {}", input.len(), output.len(), reoutput.len());
    }

    #[test]
    #[ignore]
    fn reordertest() {
        let input = array![
            [
                [111, 112, 113],
                [121, 122, 123],
                [131, 132, 133],
                [141, 142, 143]
            ],
            [
                [211, 212, 213],
                [221, 222, 223],
                [231, 232, 233],
                [241, 242, 243]
            ]
        ];
        println!("{:?}", input.clone().into_raw_vec());
        let swapped = make_ordered_copy(1, &mut Some(5), &input);
        println!("{:?}", swapped.clone().into_raw_vec());
    }

    #[test]
    fn tfm_1d_test() {
        let input = array![1., 2., 3., 4.].map(|x| Complex::new(*x, 0.));
        // array([10.+0.j, -2.+2.j, -2.+0.j, -2.-2.j])
        let expected = array![
            Complex::new(10., 0.),
            Complex::new(-2., 2.),
            Complex::new(-2., -0.),
            Complex::new(-2., -2.)
        ];
        let output = fft(&input, None, 0);

        //println!("input {:?}",input);
        //println!("expected {:?}",expected);
        //println!("output {:?}",output);

        assert_eq!(expected, output);
    }

    #[test]
    fn tfm_1dn_test() {
        let input = array![1, 2, 3, 4].map(|x| Complex::new(*x as f64, 0.));
        // array([10.+0.j, -2.+2.j, -2.+0.j, -2.-2.j])
        let expected = array![
            Complex::new(10., 0.),
            Complex::new(-2., 2.),
            Complex::new(-2., -0.),
            Complex::new(-2., -2.)
        ];
        let output = fftn(&input, None);

        println!("input {}", input);
        println!("expected {}", expected);
        //println!("output {:?}",output);

        assert_eq!(expected, output);
    }

    #[test]
    fn tfm_3d1_test() {
        let input = array![
            [
                [111, 112, 113],
                [121, 122, 123],
                [131, 132, 133],
                [141, 142, 143]
            ],
            [
                [211, 212, 213],
                [221, 222, 223],
                [231, 232, 233],
                [241, 242, 243]
            ]
        ]
        .map(|x| Complex::new(*x as f32, 0.));

        #[allow(non_upper_case_globals)]
        const j: Complex<f32> = Complex { re: 0., im: 1. };

        let expected = array![
            [
                [336. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j],
                [366. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j],
                [396. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j],
                [426. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j]
            ],
            [
                [636. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j],
                [666. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j],
                [696. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j],
                [726. + 0. * j, -1.5 + 0.8660254 * j, -1.5 - 0.8660254 * j]
            ]
        ];

        let output = fft(&input, None, 2);
        //println!("output {}",&output-&expected);

        assert_eq!(expected, output);
    }

    #[test]
    fn tfm_3dn_test() {
        let input = array![
            [
                [111, 112, 113],
                [121, 122, 123],
                [131, 132, 133],
                [141, 142, 143]
            ],
            [
                [211, 212, 213],
                [221, 222, 223],
                [231, 232, 233],
                [241, 242, 243]
            ]
        ]
        .map(|x| Complex::new(*x as f32, 0.));

        #[allow(non_upper_case_globals)]
        const j: Complex<f32> = Complex { re: 0., im: 1. };

        let expected = array![
            [
                [4248. + 0. * j, -12. + 6.92820323 * j, -12. - 6.92820323 * j],
                [-120. + 120. * j, 0. + 0. * j, 0. + 0. * j],
                [-120. + 0. * j, 0. + 0. * j, 0. + 0. * j],
                [-120. - 120. * j, 0. + 0. * j, 0. + 0. * j]
            ],
            [
                [-1200. + 0. * j, 0. + 0. * j, 0. + 0. * j],
                [0. + 0. * j, 0. + 0. * j, 0. + 0. * j],
                [0. + 0. * j, 0. + 0. * j, 0. + 0. * j],
                [0. + 0. * j, 0. + 0. * j, 0. + 0. * j]
            ]
        ];

        let output = fftn(&input, None);
        //println!("output {:?}",&output-&expected);

        assert_eq!(expected, output);
    }

    #[test]
    fn tfm_3dk_test() {
        let input = array![
            [
                [111, 112, 113],
                [121, 122, 123],
                [131, 132, 133],
                [141, 142, 143]
            ],
            [
                [211, 212, 213],
                [221, 222, 223],
                [231, 232, 233],
                [241, 242, 243]
            ]
        ]
        .map(|x| Complex::new(*x as f32, 0.));

        #[allow(non_upper_case_globals)]
        const j: Complex<f32> = Complex { re: 0., im: 1. };

        let expected = array![
            [
                [1524. + 0. * j, -6. + 3.46410162 * j, -6. - 3.46410162 * j],
                [-60. + 60. * j, 0. + 0. * j, 0. + 0. * j],
                [-60. + 0. * j, 0. + 0. * j, 0. + 0. * j],
                [-60. - 60. * j, 0. + 0. * j, 0. + 0. * j]
            ],
            [
                [2724. + 0. * j, -6. + 3.46410162 * j, -6. - 3.46410162 * j],
                [-60. + 60. * j, 0. + 0. * j, 0. + 0. * j],
                [-60. + 0. * j, 0. + 0. * j, 0. + 0. * j],
                [-60. - 60. * j, 0. + 0. * j, 0. + 0. * j]
            ]
        ];

        let output = fftk(&input, None, &[1, 2]);
        //println!("output {:?}",&output-&expected);
        assert_eq!(expected, output);
    }

    #[test]
    #[should_panic]
    fn tfm_3dk_invalid_indices() {
        let input = array![
            [
                [111, 112, 113],
                [121, 122, 123],
                [131, 132, 133],
                [141, 142, 143]
            ],
            [
                [211, 212, 213],
                [221, 222, 223],
                [231, 232, 233],
                [241, 242, 243]
            ]
        ]
        .map(|x| Complex::new(*x as f32, 0.));
        let output = fftk(&input, None, &[2, 2, 1]);
        //println!("output {:?}",&output-&expected);
        drop(output); // Ensures that output is calculated.
    }
    #[test]
    fn tfm_3dn_inverse_test() {
        let input = array![
            [
                [111, 112, 113],
                [121, 122, 123],
                [131, 132, 133],
                [141, 142, 143]
            ],
            [
                [211, 212, 213],
                [221, 222, 223],
                [231, 232, 233],
                [241, 242, 243]
            ]
        ]
        .map(|x| Complex::new(*x as f32, 0.));

        let transformed = offtn(&input, None);
        let recovered = oifftn(&transformed,None);

        let diff = &input-&recovered;
        let mut testval = Complex::new(0.,0.);
        for c in diff.iter() {
            testval += c;
        }
        testval /= diff.len() as f32;
        assert!(testval.norm_sqr() < 0.0001);
    }
}
