use crate::fft_run::{fft_inplace, rfft_bwd, rfft_fwd};
use crate::num_trailts_h::*;

use ndarray::{Array, ArrayBase, Axis, Data, Dimension, RemoveAxis, Slice};

pub fn make_ordered_copy<T: Copy + Zero, S, D>(
    d: usize,
    n: &mut Option<usize>,
    array: &ArrayBase<S, D>,
) -> Array<T, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    assert!(d < array.ndim()); // Also asserts that there is at least one axis
    let n_real = array.len_of(Axis(d));
    let n_use = match *n {
        Some(i) => i,
        None => n_real,
    };
    *n = Some(n_use);
    assert!(n_use >= n_real);
    let dmax = array.ndim() - 1;
    //array.swap_axes(d,dmax); // Put selected axes to the back
    // Now buid up result.
    let mut aview = array.view();
    aview.swap_axes(d, dmax); // Swap axis in specification
    let mut res_dim = aview.raw_dim();
    let mut res_view = res_dim.as_array_view_mut();
    res_view[dmax] = n_use; // Increase axis to desired size

    // Now just build array and assign
    let mut res = unsafe { Array::<T, D>::uninitialized(res_dim) };

    // Select ranges that need to be filled.
    let mut lower = res.slice_axis_mut(Axis(dmax), Slice::from(..n_real));
    lower.assign(&aview);
    drop(lower);

    let mut upper = res.slice_axis_mut(Axis(dmax), Slice::from(n_real..));
    upper.fill(T::zero());
    drop(upper);

    res.swap_axes(d, dmax);
    res
}

/*
// This function returns a copy of array that has a stride of 1 for dimension d
fn make_ordered_copy_old<T: Copy + Zero,S,D>(d: usize, n: &mut Option<usize>, array: &ArrayBase<S, D>) -> Array<T,D>
where S: Data<Elem=T>, D: Dimension + RemoveAxis {
    assert!(d<=array.ndim());
    let n_real = array.len_of(Axis(d));
    let n_use = match *n {Some(i) => i, None => n_real};
    *n = Some(n_use);
    assert!(n_use>=n_real);
    let n_insert =  n_use - n_real;
    let dmax = array.ndim()-1;
    //array.swap_axes(d,dmax); // Put selected axes to the back
    // Now buid up result.
    let mut res_dim = array.raw_dim();
    let mut res_view = res_dim.as_array_view_mut();
    res_view.swap(d,dmax); // Swap axis in specification
    res_view[dmax] = n_use; // Increase axis to desired size

    let blockcount = array.len()/n_real;
    assert_eq!(array.len(),blockcount*n_real);
    let tzero = T::zero();

    let mut vec_store = Vec::<T>::with_capacity(blockcount*n_use);
    for lane in array.lanes(Axis(d)) {
        lane.iter().cloned().for_each(|x| vec_store.push(x));
        for _i in 0..n_insert {
            vec_store.push(tzero);
        }
    }

    let mut res = Array::from_shape_vec(res_dim, vec_store).unwrap();
    res.swap_axes(d,dmax);
    res
}*/

/// Compute the one-dimensional discrete Fourier Transform.
pub fn _fft_fb_axis<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    mut n: Option<usize>,
    axis: usize,
    ortho: bool,
    inverse: bool,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    let mut output = make_ordered_copy(axis, &mut n, a);
    fft_inplace(
        output.as_slice_memory_order_mut().unwrap(),
        n.unwrap(),
        ortho,
        inverse,
    );
    output
}

// Compute the one-dimensional discrete real input Fouier Transform
pub fn _rfft_fwd_axis<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    mut n: Option<usize>,
    axis: usize,
    ortho: bool,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    let mut rawin = make_ordered_copy(axis, &mut n, a);
    let rawout = rfft_fwd(
        rawin.as_slice_memory_order_mut().unwrap(),
        n.unwrap(),
        ortho,
    );
    let mut outdim = a.raw_dim();
    let nval = n.unwrap(); // This is save since make_ordered_copy() sets n
    let m = match nval % 2 == 0 {
        true => 1 + nval / 2,
        false => (nval + 1) / 2,
    }; // The fft space is shorter
    outdim.as_array_view_mut()[axis] = m;
    Array::from_shape_vec(outdim, rawout).unwrap()
}

// Compute the one-dimensional discrete real input Fouier Transform
pub fn _rfft_bwd_axis<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: usize,
    axis: usize,
    ortho: bool,
) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    let m = match n % 2 == 0 {
        true => 1 + n / 2,
        false => (n + 1) / 2,
    }; // The fft space is shorter
    let mut rawin = make_ordered_copy(axis, &mut Some(m), a);
    let rawout = rfft_bwd(rawin.as_slice_memory_order_mut().unwrap(), n, ortho);
    let mut outdim = a.raw_dim();
    outdim.as_array_view_mut()[axis] = n;
    Array::from_shape_vec(outdim, rawout).unwrap()
}
