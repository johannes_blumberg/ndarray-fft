use super::fft_impl::*;
use super::num_trailts_h::*;
use crate::fft_run::fft_inplace;
use ndarray::{Array, ArrayBase, Data, Dimension, RemoveAxis};

fn is_sorted(axes: &[usize]) -> bool {
    if axes.len() < 2 {
        return true;
    };
    let mut a = axes[0];

    for b in &axes[1..] {
        if a >= *b {
            return false;
        };
        a = *b;
    }
    true
}

pub fn _fft_fb_k<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axes: &[usize],
    ortho: bool,
    inverse: bool,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    assert!(is_sorted(axes), "axes list must be sorted");
    let icnt = axes.len();
    match n {
        Some(nlist) => {
            assert_eq!(
                nlist.len(),
                icnt,
                "Specified number of axes {} missmatches given dimension {}",
                nlist.len(),
                icnt
            );
            if icnt == 0 {
                return a.to_owned();
            }
            if icnt == 1 {
                return _fft_fb_axis(a, Some(nlist[0]), axes[0], ortho, inverse);
            }

            // icnt > 1
            let mut buffer = _fft_fb_axis(a, Some(nlist[0]), axes[0], ortho, inverse);
            for i in 1..icnt {
                let mut ni = Some(nlist[i]);
                buffer = make_ordered_copy(axes[i], &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    inverse,
                );
            }

            buffer
        }
        None => {
            if icnt == 0 {
                return a.to_owned();
            }
            if icnt == 1 {
                return _fft_fb_axis(a, None, axes[0], ortho, inverse);
            }

            // icnt > 1
            let mut buffer = _fft_fb_axis(a, None, axes[0], ortho, inverse);
            for i in 1..icnt {
                let mut ni = None; // Will be fixed by the buffer creation routine
                buffer = make_ordered_copy(axes[i], &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    inverse,
                );
            }

            buffer
        }
    }
}

// Compute the one-dimensional discrete real input Fouier Transform
pub fn _rfft_fwd_k<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axes: &[usize],
    ortho: bool,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    assert!(is_sorted(axes), "axes list must be sorted");
    let icnt = axes.len();
    match n {
        Some(nlist) => {
            assert_eq!(
                nlist.len(),
                icnt,
                "Specified number of axes {} missmatches given dimension {}",
                nlist.len(),
                icnt
            );
            if icnt == 0 {
                panic!("Must specify at least one dimension")
            }
            if icnt == 1 {
                return _rfft_fwd_axis(a, Some(nlist[0]), axes[0], ortho);
            }

            // icnt > 1
            let mut buffer = _rfft_fwd_axis(a, Some(nlist[0]), axes[0], ortho);

            // Now it's just a normal Fouier Transform
            for i in 1..icnt {
                let mut ni = Some(nlist[i]);
                buffer = make_ordered_copy(axes[i], &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    false,
                );
            }

            buffer
        }
        None => {
            if icnt == 0 {
                panic!("Must specify at least one dimension")
            }
            if icnt == 1 {
                return _rfft_fwd_axis(a, None, axes[0], ortho);
            }

            // icnt > 1
            let mut buffer = _rfft_fwd_axis(a, None, axes[0], ortho);

            // Now it's just a normal Fouier Transform

            // However we handcode it to avoid using an other buffer
            for i in 1..icnt {
                let mut ni = None; // Will be selected by the buffer creation routine
                buffer = make_ordered_copy(axes[i], &mut ni, &buffer);
                fft_inplace(
                    buffer.as_slice_memory_order_mut().unwrap(),
                    ni.unwrap(),
                    ortho,
                    false,
                );
            }

            buffer
        }
    }
}

// Compute the one-dimensional discrete real input Fouier Transform
pub fn _rfft_bwd_k<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: &[usize],
    axes: &[usize],
    ortho: bool,
) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    assert!(is_sorted(axes), "axes list must be sorted");
    let icnt = axes.len();
    assert_eq!(
        n.len(),
        icnt,
        "Specified number of axes {} missmatches given dimension {}",
        n.len(),
        icnt
    );
    //let nlast = n[n.len()-1]; // The last value should be specified otherwise panic.
    if icnt == 0 {
        panic!("Must specify at least one dimension")
    }
    if icnt == 1 {
        return _rfft_bwd_axis(a, n[0], axes[0], ortho);
    }

    // More dimensions

    // First transform all dimensions exept the first one normally
    let buffer = _fft_fb_k(a, Some(&n[1..]), &axes[1..], ortho, true);

    // Compute complex to real fft
    _rfft_bwd_axis(&buffer, n[0], axes[0], ortho)
}

/// Applies a discrete Fouier Transform along a specified set of axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1.
///
/// This function should be used together with [`ifftk`]
pub fn fftk<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axis: &[usize],
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_k(a, n, axis, false, false)
}

/// Applies a discrete Inverse Fouier Transform along a specified set of axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/n, where n is the number of elements along this axis.
///  
/// This function should be used together with [`fftk`]
pub fn ifftk<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axis: &[usize],
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_k(a, n, axis, false, true)
}

/// Applies a orthonormal discrete Fouier Transform along a specified set of axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`oifftk`]
pub fn offtk<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axis: &[usize],
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_k(a, n, axis, true, false)
}

/// Applies a orthonormal discrete inverse Fouier Transform along a specified set of axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`offtk`]
pub fn oifftk<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axis: &[usize],
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_k(a, n, axis, true, true)
}

// Real Space Transform

/// Applies a discrete Real Input Fouier Transform along a specified set of axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1.
///
/// The real to complex transform is calculated along the first axis specified in axes,
/// and the vector lengh along this axis is reduced correspondingly for the output.
/// If k is the lengh of the corresponding vector in the input arry,
/// the resulting lengh is 1+k/2 if k is even and (k+1)/2 if k is odd.
///
/// This function should be used together with [`irfftk`]
pub fn rfftk<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axis: &[usize],
) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    _rfft_fwd_k(a, n, axis, false)
}

/// Applies a discrete Inverse Real Input Fouier Transform along a specified set of axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The parameter n indecates the assumed vector lengh along the
/// transformation axes. It should specify the lengh for each transformation
/// dimension in the order given by axes as the show up in the resulting array.
/// The normalisation is taken to be 1/n, where n is the number of elements along this axis.
///
/// It is assumend that the first axis specified is the one that has been transformed using the real to complex
/// transform.
///  
/// This function should be used together with [`rfftk`]
pub fn irfftk<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: &[usize], axis: &[usize]) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _rfft_bwd_k(a, n, axis, false)
}

/// Applies a orthonormal discrete Fouier Transform along a specified set of axes
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The optional parameter n indecates the assumed vector lengh along the
/// transformation axes. If Some is given, it should specify the lengh for each transformation
/// dimension in the order given by axes, if None is given the corresponding lengh is determined,
/// by the input array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// The real to complex transform is calculated along the first axis specified in axes,
/// and the vector lengh along this axis is reduced correspondingly for the output.
/// If k is the lengh of the corresponding vector in the input arry,
/// the resulting lengh is 1+k/2 if k is even and (k+1)/2 if k is odd.
///
/// This function should be used together with [`oirfftk`]
pub fn orfftk<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<&[usize]>,
    axis: &[usize],
) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    _rfft_fwd_k(a, n, axis, true)
}

/// Applies a orthonormal discrete inverse Fouier Transform along a specified axis and returns the result
///
/// The axes along which the Fouier Transform should be performed are given in the
/// axes parameter. The parameter n indecates the assumed vector lengh along the
/// transformation axes. It should specify the lengh for each transformation
/// dimension in the order given by axes as the show up in the resulting array.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// It is assumend that the first axis specified is the one that has been transformed using the real to complex
/// transform.
///
/// This function should be used together with [`orfftk`]
pub fn oirfftk<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: &[usize], axis: &[usize]) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _rfft_bwd_k(a, n, axis, false)
}
