use crate::num_trailts_h::*;

use chfft::*;

//type ArrayBase1D<S> = ArrayBase<S,Ix1>;

fn fft_get_norm<T: FFTnum>(n: usize, ortho: bool, inverse: bool) -> T {
    match ortho {
        true => T::from_usize(n).unwrap().recip().sqrt(),
        false => match inverse {
            true => T::from_usize(n).unwrap().recip(),
            false => T::one(),
        },
    }
}

pub fn fft_inplace<T: FFTnum>(io: &mut [Complex<T>], n: usize, ortho: bool, inverse: bool) {
    // This function performs a inplace FFT of multiple blocks with lengh n stored in a buffer io
    assert_eq!(
        io.len() % n,
        0,
        "io buffer len {} doesn't fit vector len {}",
        io.len(),
        n
    );
    let c = io.len() / n;
    let norm = fft_get_norm::<T>(n, ortho, inverse);
    let mut fft_inst = CFft1D::with_len(n);
    for i in 0..c {
        // Get current vector
        let io_sub = &mut io[i * n..i * n + n];
        match inverse {
            true => fft_inst.backward0i(io_sub),
            false => fft_inst.forward0i(io_sub),
        };
    }
    io.iter_mut().for_each(|el| *el *= norm);
}

pub fn rfft_fwd<T: FFTnum>(input: &[T], n: usize, ortho: bool) -> Vec<Complex<T>> {
    // This function performs a FFT of blocks with lengh n stored in a buffer input
    // Results are stored and returned in an output buffer.
    let m = match n % 2 == 0 {
        true => 1 + n / 2,
        false => (n + 1) / 2,
    }; // The fft space is shorter
    assert_eq!(
        input.len() % n,
        0,
        "io buffer len {} doesn't fit vector len {}",
        input.len(),
        n
    );
    let c = input.len() / n;
    let mut outbuff = Vec::<Complex<T>>::with_capacity(c * m); //
    let norm = fft_get_norm::<T>(n, ortho, false);
    let mut fft_inst = RFft1D::new(n);
    for i in 0..c {
        // Get current vector
        let io_sub = &input[i * n..i * n + n];
        outbuff.append(&mut fft_inst.forward0(io_sub));
    }
    outbuff.iter_mut().for_each(|el| *el *= norm);
    outbuff
}

pub fn rfft_bwd<T: FFTnum>(input: &[Complex<T>], n: usize, ortho: bool) -> Vec<T> {
    // This function performs a FFT of blocks with lengh n stored in a buffer input
    // Results are stored and returned in an output buffer.
    let m = match n % 2 == 0 {
        true => 1 + n / 2,
        false => (n + 1) / 2,
    }; // The fft space is shorter
    assert_eq!(
        input.len() % m,
        0,
        "io buffer len {} doesn't fit vector len {}",
        input.len(),
        m
    );
    let c = input.len() / m;
    let mut outbuff = Vec::<T>::with_capacity(c * n); //
    let norm = fft_get_norm::<T>(n, ortho, true);
    let mut fft_inst = RFft1D::new(n);
    for i in 0..c {
        // Get current vector
        let io_sub = &input[i * m..i * m + m];
        outbuff.append(&mut fft_inst.backward0(io_sub));
    }
    outbuff.iter_mut().for_each(|el| *el *= norm);
    outbuff
}

/*
pub fn fft_fb<T: FFTnum>(input: &[Complex<T>], n: usize, vcache: &mut [Vec<Complex<T>>], ortho: bool, inverse: bool) {
    // This function performs a FFT of blocks with lengh n stored in a buffer io
    // Results are shored vectorwise in vcache
    let c = vcache.len();
    assert_eq!(input.len()%n,0,"io buffer len {} doesn't fit vector len {}",input.len(),n);
    assert!(c*n==input.len(),"io buffer lengh {} doesn't match exectations for {} vectors of len {}",input.len(),c,n);
    let norm = fft_get_norm::<T>(n, ortho, false);
    let mut fft_inst = CFft1D::with_len(n);
    for (i,wbuff) in vcache.iter_mut().enumerate() {
        let io_sub = &input[i*n..i*n+n];
        *wbuff = match inverse{
            true => fft_inst.backward0(io_sub),
            false => fft_inst.forward0(io_sub)
        };
        wbuff.iter_mut().for_each(|el| *el *= norm);
    }
}

pub fn rfft_fwd<T: FFTnum>(input: &[T], n: usize, vcache: &mut [Vec<Complex<T>>], ortho: bool) {
    // This function performs a FFT of blocks with lengh n stored in a buffer iput
    // Results are shored vectorwise in vcache
    let c = vcache.len();
    assert_eq!(input.len()%n,0,"io buffer len {} doesn't fit vector len {}",input.len(),n);
    assert!(c*n==input.len(),"io buffer lengh {} doesn't match exectations for {} vectors of len {}",input.len(),c,n);
    let norm = fft_get_norm::<T>(n, ortho, false);
    let mut fft_inst = RFft1D::new(n);
    for (i,wbuff) in vcache.iter_mut().enumerate() {
        let io_sub = &input[i*n..i*n+n];
        *wbuff = fft_inst.forward0(io_sub);
        wbuff.iter_mut().for_each(|el| *el *= norm);
    }
}

pub fn rfft_bwd<T: FFTnum>(input: &[Complex<T>], n: usize, vcache: &mut [Vec<T>], ortho: bool) {
    // This function performs a FFT of blocks with lengh n stored in a buffer io
    // Results are shored vectorwise in vcache
    let c = vcache.len();
    let m = match n%2==0 {true => 1+n/2, false => (n+1)/2}; // The fft space lacks in values
    assert_eq!(input.len()%m,0,"io buffer len {} doesn't fit vector len {}",input.len(),n);
    assert!(c*m==input.len(),"io buffer lengh {} doesn't match exectations for {} vectors of len {}",input.len(),c,n);
    let norm = fft_get_norm::<T>(n, ortho, false);
    let mut fft_inst = RFft1D::new(n);
    for (i,wbuff) in vcache.iter_mut().enumerate() {
        let io_sub = &input[i*m..i*m+m];
        *wbuff = fft_inst.backward0(io_sub);
        wbuff.iter_mut().for_each(|el| *el *= norm);
    }
}*/
