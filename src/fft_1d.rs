use super::fft_impl::*;
use super::num_trailts_h::*;
use ndarray::{Array, ArrayBase, Data, Dimension, RemoveAxis};

/// Applies a discrete Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set
/// to be k, if Some(k) is specified. This is achived by adding zeros to the input vector.
/// If n is None the size of the corresponding dimension is taken
/// The normalisation is taken to be 1.
///
/// This function should be used together with [`ifft`]
pub fn fft<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<usize>,
    axis: usize,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_axis(a, n, axis, false, false)
}

/// Applies a discrete Inverse Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set
/// to be k, if Some(k) is specified. This is achived by adding zeros to the input vector.
/// If n is None the size of the corresponding dimension is taken
/// The normalisation is taken to be 1/n, where n is the number of elements along this axis.
///  
/// This function should be used together with [`fft`]
pub fn ifft<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<usize>,
    axis: usize,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_axis(a, n, axis, false, true)
}

/// Applies a orthonormal discrete Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set
/// to be k, if Some(k) is specified. This is achived by adding zeros to the input vector.
/// If n is None the size of the corresponding dimension is taken
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`oifft`]
pub fn offt<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<usize>,
    axis: usize,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_axis(a, n, axis, true, false)
}

/// Applies a orthonormal discrete inverse Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set
/// to be k, if Some(k) is specified. This is achived by adding zeros to the input vector.
/// If n is None the size of the corresponding dimension is taken
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`offt`]
pub fn oifft<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<usize>,
    axis: usize,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _fft_fb_axis(a, n, axis, true, true)
}

// Real Space Transform

/// Applies a discrete Real Input Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set
/// to be k, if Some(k) is specified. This is achived by adding zeros to the input vector.
/// If n is None the size of the corresponding dimension is taken.
/// The normalisation is taken to be 1.
///
/// This function should be used together with [`irfft`]
pub fn rfft<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<usize>,
    axis: usize,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    _rfft_fwd_axis(a, n, axis, false)
}

/// Applies a discrete Inverse Real Input Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set after the transformation.
/// Missing entries are padded with zero.
/// The normalisation is taken to be 1/n, where n is the number of elements along this axis.
///  
/// This function should be used together with [`rfft`]
pub fn irfft<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: usize, axis: usize) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _rfft_bwd_axis(a, n, axis, false)
}

/// Applies a orthonormal discrete Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set
/// to be k, if Some(k) is specified. This is achived by adding zeros to the input vector.
/// If n is None the size of the corresponding dimension is taken./// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`oirfft`]
pub fn orfft<T: FFTnum, D, S>(
    a: &ArrayBase<S, D>,
    n: Option<usize>,
    axis: usize,
) -> Array<Complex<T>, D>
where
    S: Data<Elem = T>,
    D: Dimension + RemoveAxis,
{
    _rfft_fwd_axis(a, n, axis, true)
}

/// Applies a orthonormal discrete inverse Fouier Transform along a specified axis and returns the result
///
/// The parameter n specifies the assumed lengh of a single transformed data set after the transformation.
/// Missing entries are padded with zero.
/// The normalisation is taken to be 1/sqrt(n), where n is the number of elements along this axis.
/// This makes the transformation orthonormal.
///
/// This function should be used together with [`orfft`]
pub fn oirfft<T: FFTnum, D, S>(a: &ArrayBase<S, D>, n: usize, axis: usize) -> Array<T, D>
where
    S: Data<Elem = Complex<T>>,
    D: Dimension + RemoveAxis,
{
    _rfft_bwd_axis(a, n, axis, false)
}
